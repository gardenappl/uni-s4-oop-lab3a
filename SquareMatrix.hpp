#pragma once

#include <iostream>

#include "MatrixMultiplyStrategy.hpp"

template<typename T>
class SquareMatrixManaged;


//Strassen (and parallel) multiplication requires splitting a matrix into sub-matrices
//Instead of splitting, we make pointers using offset_x and offset_y

//This does NOT manage memory!

template<typename T>
class SquareMatrix
{
protected:
	T* array;
	int size;
	int offset_x;
	int offset_y;
	int array_width;

public:

	SquareMatrix(T* array, int size, int offset_x = 0, int offset_y = 0,
		int array_width = -1)
		: array(array), size(size), offset_x(offset_x), offset_y(offset_y)
	{
		if (array_width == -1)
			this->array_width = size;
		else
			this->array_width = array_width;
	}

	//NOT virtual, this is for efficiency

	T& get(int x, int y)
	{
		return array[get_index(x, y)];
	}

	const T& get(int x, int y) const
	{
		return array[get_index(x, y)];
	}

	int get_size() const
	{
		return size;
	}

	void print(std::ostream& os) const
	{
		for (int y = 0; y < size; y++)
		{
			for (int x = 0; x < size; x++)
			{
				os << array[get_index(x, y)] << ' ';
			}
			os << '\n';
		}
	}

	static SquareMatrixManaged<T>* add(const SquareMatrix* m1, const SquareMatrix* m2)
	{
		SquareMatrixManaged<T>* result = new SquareMatrixManaged(m1->size);
		add(m1, m2, m1);
		return result;
	}

	void add(const SquareMatrix* m2)
	{
		int size = get_size();
		for (int x = 0; x < size; x++)
		{
			for (int y = 0; y < size; y++)
			{
				get(x, y) += m2->get(x, y);
			}
		}
	}

	friend bool operator==(const SquareMatrix<T>& m1, const SquareMatrix<T>& m2)
	{
		if (m1.size != m2.size)
			return false;

		for (int x = 0; x < m1.size; x++)
		{
			for (int y = 0; y < m1.size; y++)
			{
				if (m1.get(x, y) != m2.get(x, y))
				{
					std::cout << m1.get(x, y) << " does not equal " << m2.get(x, y);
					return false;
				}
			}
		}
		return true;
	}

	friend bool operator!=(const SquareMatrix<T>& m1, const SquareMatrix<T>& m2)
	{
		return !(m1 == m2);
	}

	static SquareMatrix* multiply(const SquareMatrix* m1, const SquareMatrix* m2, MatrixMultiplyStrategy<T>* strategy)
	{
		SquareMatrixManaged<T>* result = new SquareMatrixManaged<T>(m1->size);
		strategy->multiply(m1, m2, result);
		return result;
	}

	static void subtract(const SquareMatrix* m1, const SquareMatrix* m2, SquareMatrix* result)
	{
		for (int x = 0; x < result->size; x++)
		{
			for (int y = 0; y < result->size; y++)
			{
				result->get(x, y) = m1->get(x, y) - m2->get(x, y);
			}
		}
	}

	void subtract(const SquareMatrix& m2)
	{
		subtract(m2, this);
	}

	SquareMatrix* bottom_right() const
	{
		return new SquareMatrix(array, size / 2, offset_x + size / 2, offset_y + size / 2, array_width);
	}

	SquareMatrix* top_right() const
	{
		return new SquareMatrix(array, size / 2, offset_x + size / 2, offset_y, array_width);
	}

	SquareMatrix* bottom_left() const
	{
		return new SquareMatrix(array, size / 2, offset_x, offset_y + size / 2, array_width);
	}

	SquareMatrix* top_left() const
	{
		return new SquareMatrix(array, size / 2, offset_x, offset_y, array_width);
	}


	virtual ~SquareMatrix() {}

protected:
	size_t get_index(int x, int y) const
	{
		return array_width * (offset_y + y) + (offset_x + x);
	}

	void clear()
	{
		for (int y = offset_y; y < offset_y + size; y++)
		{
			//memset(array + (offset_x + array_width * y), 0, sizeof(T) * size);
			for (int x = offset_x; x < offset_x + size; x++)
				get(x, y) = 0;
		}
	}

	static void add(const SquareMatrix* m1, const SquareMatrix* m2, SquareMatrix* result)
	{
		//std::cout << "Adding" << std::endl;
		int size = result->get_size();
		for (int x = 0; x < size; x++)
		{
			for (int y = 0; y < size; y++)
			{
				result->get(x, y) = m1->get(x, y) + m2->get(x, y);
			}
		}
	}

	static void multiply(const SquareMatrix* m1, const SquareMatrix* m2, SquareMatrix* result, MatrixMultiplyStrategy<T>* strategy)
	{
		strategy->multiply(m1, m2, result);
	}

    friend MatrixMultiplyStrategy<T>;
    friend MatrixMultiplyStrategyNaive<T>;
    friend MatrixMultiplyStrategyStrassen<T>;
};



//This DOES manage its own memory!

template<typename T>
class SquareMatrixManaged : public SquareMatrix<T>
{
public:
	SquareMatrixManaged(SquareMatrixManaged&& m)
		: array(m.array), size(m.size), offset_x(m.offset_x), offset_y(m.offset_y), array_width(m.array_width)
	{
		m.array = nullptr;
	}

	SquareMatrixManaged(T* array, int size)
		: SquareMatrix(array, size)
	{}

	SquareMatrixManaged(int size)
		: SquareMatrix(new T[size * size], size)
	{}

	static SquareMatrixManaged* allocate_all_zeroes(int size)
	{
		SquareMatrixManaged* matrix = new SquareMatrixManaged(size);
		matrix->clear();
		//memset(matrix->array, 0, sizeof(T) * size * size);
		return matrix;
	}

	~SquareMatrixManaged()
	{
		delete[] array;
	}
};