#include <iostream>
#include <random>
#include <memory>
#include <chrono>

#include "SquareMatrix.hpp"
#include "MatrixMultiplyStrategy.hpp"

using namespace std;



int main()
{
	const int size = 512;

    std::cout << "Using " << std::thread::hardware_concurrency() << " threads, "
		<< size << " x " << size << std::endl;

	std::unique_ptr<SquareMatrix<int>> a(SquareMatrixManaged<int>::allocate_all_zeroes(size));
	std::unique_ptr<SquareMatrix<int>> b(SquareMatrixManaged<int>::allocate_all_zeroes(size));



	std::unique_ptr<MatrixMultiplyStrategy<int>> strategy(new MatrixMultiplyStrategyNaive<int>());

	std::unique_ptr<SquareMatrix<int>> result(SquareMatrixManaged<int>::allocate_all_zeroes(size));

	cout << "Standard multiplication... ";

	auto start = std::chrono::high_resolution_clock::now();
	result.reset(SquareMatrix<int>::multiply(a.get(), b.get(), strategy.get()));
	auto end = std::chrono::high_resolution_clock::now();

	std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::endl;




	std::unique_ptr<MatrixMultiplyStrategy<int>> parallelStrategy
	(
		new MatrixMultiplyStrategyParallel<int>(strategy.get())
	);

	result.reset(SquareMatrixManaged<int>::allocate_all_zeroes(size));

	cout << "Parallel multiplication (using Standard)... ";

	start = std::chrono::high_resolution_clock::now();
	result.reset(SquareMatrix<int>::multiply(a.get(), b.get(), parallelStrategy.get()));
	end = std::chrono::high_resolution_clock::now();

	std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::endl;




	strategy.reset(new MatrixMultiplyStrategyStrassen<int>());

	cout << "Strassen multiplication... ";

	start = std::chrono::high_resolution_clock::now();
	result.reset(SquareMatrix<int>::multiply(a.get(), b.get(), strategy.get()));
	end = std::chrono::high_resolution_clock::now();

	std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::endl;




	parallelStrategy.reset(new MatrixMultiplyStrategyParallel<int>(strategy.get()));

	result.reset(SquareMatrixManaged<int>::allocate_all_zeroes(size));

	cout << "Parallel multiplication (using Strassen)... ";

	start = std::chrono::high_resolution_clock::now();
	result.reset(SquareMatrix<int>::multiply(a.get(), b.get(), parallelStrategy.get()));
	end = std::chrono::high_resolution_clock::now();

	std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::endl;



    return 0;
}
