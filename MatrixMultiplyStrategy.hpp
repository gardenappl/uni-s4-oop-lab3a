#pragma once

#include <thread>
#include <memory>

template<typename T>
class SquareMatrix;

template<typename T>
class SquareMatrixManaged;


template<typename T>
class MatrixMultiplyStrategy
{
public:
	//m1 and m2 are assumed to have same size
	virtual void multiply(const SquareMatrix<T>* m1, const SquareMatrix<T>* m2, SquareMatrix<T>* mResult) = 0;
};

template<typename T>
class MatrixMultiplyStrategyNaive : public MatrixMultiplyStrategy<T>
{
public:
	void multiply(const SquareMatrix<T>* m1, const SquareMatrix<T>* m2, SquareMatrix<T>* mResult) override
	{
		int size = m1->get_size();
		for (int x = 0; x < size; x++)
		{
			for (int y = 0; y < size; y++)
			{
				T& result_element = mResult->get(x, y);
				result_element = 0;
				for (int i = 0; i < size; i++)
				{
					result_element += m1->get(i, y) * m2->get(x, i);
				}
			}
		}
	}
};

template<typename T>
class MatrixMultiplyStrategyStrassen : public MatrixMultiplyStrategy<T>
{
public:
	MatrixMultiplyStrategyStrassen()
		: naive(new MatrixMultiplyStrategyNaive<T>())
	{}

	//Assume that matrix size is 2^n
	void multiply(const SquareMatrix<T>* a, const SquareMatrix<T>* b, SquareMatrix<T>* result) override
	{
		if (a->get_size() < 100)
		{
			naive->multiply(a, b, result);
			return;
		}

		int split_size = a->size / 2;
		if (split_size * 2 != a->size)
			throw std::invalid_argument("Bad matrix size");

		std::unique_ptr<SquareMatrix<T>> a11(a->top_left());
		std::unique_ptr<SquareMatrix<T>> a12(a->top_right());
		std::unique_ptr<SquareMatrix<T>> a21(a->bottom_left());
		std::unique_ptr<SquareMatrix<T>> a22(a->bottom_right());

		std::unique_ptr<SquareMatrix<T>> b11(b->top_left());
		std::unique_ptr<SquareMatrix<T>> b12(b->top_right());
		std::unique_ptr<SquareMatrix<T>> b21(b->bottom_left());
		std::unique_ptr<SquareMatrix<T>> b22(b->bottom_right());

		std::unique_ptr<SquareMatrix<T>> c11(result->top_left());
		std::unique_ptr<SquareMatrix<T>> c12(result->top_right());
		std::unique_ptr<SquareMatrix<T>> c21(result->bottom_left());
		std::unique_ptr<SquareMatrix<T>> c22(result->bottom_right());

		SquareMatrixManaged<T> temp1(split_size);
		SquareMatrixManaged<T> temp2(split_size);

		SquareMatrixManaged<T> p1(split_size);
		SquareMatrixManaged<T> p2(split_size);
		SquareMatrixManaged<T> p3(split_size);
		SquareMatrixManaged<T> p4(split_size);
		SquareMatrixManaged<T> p5(split_size);
		SquareMatrixManaged<T> p6(split_size);
		SquareMatrixManaged<T> p7(split_size);

		//p1 = (a11 + a22) * (b11 + b22)
		SquareMatrix<T>::add(a11.get(), a22.get(), &temp1);
		SquareMatrix<T>::add(b11.get(), b22.get(), &temp2);
		multiply(&temp1, &temp2, &p1);

		//p2 = (a21 + a22) * b11
		SquareMatrix<T>::add(a21.get(), a22.get(), &temp1);
		multiply(&temp1, b11.get(), &p2);

		//p3 = a11 * (b12 - b22)
		SquareMatrix<T>::subtract(b12.get(), b22.get(), &temp1);
		multiply(a11.get(), &temp1, &p3);

		//p4 = a22 * (b21 - b11)
		SquareMatrix<T>::subtract(b21.get(), b11.get(), &temp1);
		multiply(a22.get(), &temp1, &p4);

		//p5 = (a11 + a12) * b22
		SquareMatrix<T>::add(a11.get(), a12.get(), &temp1);
		multiply(&temp1, b22.get(), &p5);

		//p6 = (a21 - a11) * (b11 + b12)
		SquareMatrix<T>::subtract(a21.get(), a11.get(), &temp1);
		SquareMatrix<T>::add(b11.get(), b12.get(), &temp2);
		multiply(&temp1, &temp2, &p6);

		//p7 = (a12 - a22) * (b21 + b22)
		SquareMatrix<T>::subtract(a12.get(), a22.get(), &temp1);
		SquareMatrix<T>::add(b21.get(), b22.get(), &temp2);
		multiply(&temp1, &temp2, &p7);


		//c11 = p1 + p4 - p5 + p7
		SquareMatrix<T>::add(&p1, &p4, c11.get());
		SquareMatrix<T>::subtract(c11.get(), &p5, c11.get());
		SquareMatrix<T>::add(c11.get(), &p7, c11.get());

		//c12 = p3 + p5
		SquareMatrix<T>::add(&p3, &p5, c12.get());

		//c21 = p2 + p4
		SquareMatrix<T>::add(&p2, &p4, c21.get());

		//c22 = p1 - p2 + p3 + p6
		SquareMatrix<T>::subtract(&p1, &p2, c22.get());
		SquareMatrix<T>::add(c22.get(), &p3, c22.get());
		SquareMatrix<T>::add(c22.get(), &p6, c22.get());

		//	print_matrix(c11);
		//	print_matrix(c12);
		//	print_matrix(c21);
		//	print_matrix(c22);
	}

private:
	MatrixMultiplyStrategyNaive<T>* naive;
};


template<typename T>
class MatrixMultiplyStrategyParallel : public MatrixMultiplyStrategy<T>
{
public:
	MatrixMultiplyStrategyParallel(MatrixMultiplyStrategy<T>* base_strategy, unsigned int useThreads)
		: base_strategy(base_strategy), available_threads(useThreads)
	{}

	MatrixMultiplyStrategyParallel(MatrixMultiplyStrategy<T>* base_strategy)
		: base_strategy(base_strategy), available_threads(std::thread::hardware_concurrency())
	{}

	void multiply(const SquareMatrix<T>* m1, const SquareMatrix<T>* m2, SquareMatrix<T>* mResult) override
	{
		if (available_threads == 1)
			base_strategy->multiply(m1, m2, mResult);
		multiply_internal(m1, m2, mResult, available_threads);
	}
private:
	MatrixMultiplyStrategy<T>* base_strategy;
	unsigned int available_threads;

	void multiply_internal(const SquareMatrix<T>* a, const SquareMatrix<T>* b, SquareMatrix<T>* result, int available_threads)
	{
		if (result->get_size() == 1)
		{
			result->get(0, 0) = a->get(0, 0) * b->get(0, 0);
			return;
		}

		int size = a->get_size();
		std::unique_ptr<SquareMatrix<T>> mTemp(SquareMatrixManaged<T>::allocate_all_zeroes(size));

		std::unique_ptr<MatrixMultiplyStrategy<T>> strategyUniquePtr;
		MatrixMultiplyStrategy<T>* strategy;

		available_threads /= 8;
		if (available_threads > 0)
		{
			strategy = new MatrixMultiplyStrategyParallel(base_strategy, available_threads);
			strategyUniquePtr.reset(strategy);
		}
		else
		{
			strategy = base_strategy;
		}

		std::thread threads[8];

		if (a->get_size() % 2 != 0)
			throw std::invalid_argument("Bad matrix size");

		std::unique_ptr<SquareMatrix<T>> a11(a->top_left());
		std::unique_ptr<SquareMatrix<T>> a12(a->top_right());
		std::unique_ptr<SquareMatrix<T>> a21(a->bottom_left());
		std::unique_ptr<SquareMatrix<T>> a22(a->bottom_right());

		std::unique_ptr<SquareMatrix<T>> b11(b->top_left());
		std::unique_ptr<SquareMatrix<T>> b12(b->top_right());
		std::unique_ptr<SquareMatrix<T>> b21(b->bottom_left());
		std::unique_ptr<SquareMatrix<T>> b22(b->bottom_right());

		std::unique_ptr<SquareMatrix<T>> c11(result->top_left());
		std::unique_ptr<SquareMatrix<T>> c12(result->top_right());
		std::unique_ptr<SquareMatrix<T>> c21(result->bottom_left());
		std::unique_ptr<SquareMatrix<T>> c22(result->bottom_right());

		std::unique_ptr<SquareMatrix<T>> t11(mTemp->top_left());
		std::unique_ptr<SquareMatrix<T>> t12(mTemp->top_right());
		std::unique_ptr<SquareMatrix<T>> t21(mTemp->bottom_left());
		std::unique_ptr<SquareMatrix<T>> t22(mTemp->bottom_right());

		threads[0] = std::thread([&a11, &b11, &c11, strategy]()
		{
			strategy->multiply(a11.get(), b11.get(), c11.get());
		});
		threads[1] = std::thread([&a11, &b12, &c12, strategy]()
		{
			strategy->multiply(a11.get(), b12.get(), c12.get());
		});
		threads[2] = std::thread([&a21, &b11, &c21, strategy]()
		{
			strategy->multiply(a21.get(), b11.get(), c21.get());
		});
		threads[3] = std::thread([&a21, &b12, &c22, strategy]()
		{
			strategy->multiply(a21.get(), b12.get(), c22.get());
		});
		threads[4] = std::thread([&a12, &b21, &t11, strategy]()
		{
			strategy->multiply(a12.get(), b21.get(), t11.get());
		});
		threads[5] = std::thread([&a12, &b22, &t12, strategy]()
		{
			strategy->multiply(a12.get(), b22.get(), t12.get());
		});
		threads[6] = std::thread([&a22, &b21, &t21, strategy]()
		{
			strategy->multiply(a22.get(), b21.get(), t21.get());
		});
		threads[7] = std::thread([&a22, &b22, &t22, strategy]()
		{
			strategy->multiply(a22.get(), b22.get(), t22.get());
		});

		for (size_t i = 0; i < 8; i++)
			threads[i].join();

		result->add(mTemp.get());
	}
};