#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "doctest.hpp"

#include <random>
#include <thread>
#include "SquareMatrix.hpp"
#include "MatrixMultiplyStrategy.hpp"


const int LARGE_MATRIX_SIZE = 256;
const int BAD_MATRIX_SIZE = 201;


template<typename T>
SquareMatrixManaged<T> make_random_matrix_int(int size, T min, T max)
{
	static std::default_random_engine rng_engine;
	std::uniform_int_distribution<T> element_rng(min, max);

	T* matrix = new T[size * size];
	for (size_t i = 0; i < size * size; i++)
	{
		matrix[i] = element_rng(rng_engine);
	}
	return SquareMatrixManaged<T>(matrix, size);
}

void test_small(MatrixMultiplyStrategy<int>* strategy)
{
	int* arr1 = new int[16]
		{
			1, -2, 5, 4,
			-1, 2, -5, 1,
			0, -1, -3, -2,
			-2, 3, -5, 0
		};
	int* arr2 = new int[16]
		{
			-3, -2, -3, 2,
			1, 0, 2, 3,
			1, -2, -4, 2,
			-4, 1, 1, -5
		};
	SquareMatrixManaged<int> m1(arr1, 4);
	SquareMatrixManaged<int> m2(arr2, 4);

	int* arrResult = new int[16]
		{
			-16, -8, -23, -14,
			-4, 13, 28, -11,
			4, 4, 8, 1,
			4, 14, 32, -5
		};

	SquareMatrixManaged<int> mResult(arrResult, 4);
	std::unique_ptr<SquareMatrix<int>> realResult(SquareMatrix<int>::multiply(&m1, &m2, strategy));
	REQUIRE(mResult == *realResult);
}

TEST_CASE("Test small")
{
    SUBCASE("Naive")
    {
		std::unique_ptr<MatrixMultiplyStrategy<int>> strategy(new MatrixMultiplyStrategyNaive<int>());
		test_small(strategy.get());
    }
	SUBCASE("Strassen")
	{
		std::unique_ptr<MatrixMultiplyStrategy<int>> strategy(new MatrixMultiplyStrategyStrassen<int>());
		test_small(strategy.get());
	}
	SUBCASE("Parallel, suggested threads: 1000000")
	{
		std::unique_ptr<MatrixMultiplyStrategy<int>> baseStrategy(new MatrixMultiplyStrategyNaive<int>());
		std::unique_ptr<MatrixMultiplyStrategy<int>> strategy(new MatrixMultiplyStrategyParallel<int>(baseStrategy.get()));
		test_small(strategy.get());
	}
}

TEST_CASE("Bad matrix size")
{
	SquareMatrixManaged<long> m1(make_random_matrix_int<long>(BAD_MATRIX_SIZE, -100, 100));
	SquareMatrixManaged<long> m2(make_random_matrix_int<long>(BAD_MATRIX_SIZE, -100, 100));

	SUBCASE("Strassen")
	{
		std::unique_ptr<MatrixMultiplyStrategy<long>> strategy(new MatrixMultiplyStrategyStrassen<long>());
		CHECK_THROWS_WITH(SquareMatrix<long>::multiply(&m1, &m2, strategy.get()), "Bad matrix size");
	}

	SUBCASE("Parallel, suggested threads: 8")
	{
		std::unique_ptr<MatrixMultiplyStrategy<long>> baseStrategy(new MatrixMultiplyStrategyNaive<long>());
		std::unique_ptr<MatrixMultiplyStrategy<long>> strategy(new MatrixMultiplyStrategyParallel<long>(baseStrategy.get(), 8));

		CHECK_THROWS_WITH(SquareMatrix<long>::multiply(&m1, &m2, strategy.get()), "Bad matrix size");
	}
}

TEST_CASE("Compare with Naive")
{
	SquareMatrixManaged<long> m1(make_random_matrix_int<long>(LARGE_MATRIX_SIZE, -100, 100));
	SquareMatrixManaged<long> m2(make_random_matrix_int<long>(LARGE_MATRIX_SIZE, -100, 100));

	std::unique_ptr<MatrixMultiplyStrategy<long>> naiveStrategy(new MatrixMultiplyStrategyNaive<long>());
	std::unique_ptr<SquareMatrix<long>> naiveResult(SquareMatrix<long>::multiply(&m1, &m2, naiveStrategy.get()));

	SUBCASE("Strassen")
	{
		std::unique_ptr<MatrixMultiplyStrategy<long>> strategy(new MatrixMultiplyStrategyStrassen<long>());
		std::unique_ptr<SquareMatrix<long>> result(SquareMatrix<long>::multiply(&m1, &m2, strategy.get()));

		CHECK(*result == *naiveResult);
	}

	SUBCASE("Parallel, base strategy: Standard")
	{
		std::unique_ptr<MatrixMultiplyStrategy<long>> baseStrategy(new MatrixMultiplyStrategyNaive<long>());
		std::unique_ptr<MatrixMultiplyStrategy<long>> strategy(new MatrixMultiplyStrategyParallel<long>(baseStrategy.get()));

		std::unique_ptr<SquareMatrix<long>> result(SquareMatrix<long>::multiply(&m1, &m2, strategy.get()));

		CHECK(*result == *naiveResult);
	}
	SUBCASE("Parallel, base strategy: Strassen")
	{
		std::unique_ptr<MatrixMultiplyStrategy<long>> baseStrategy(new MatrixMultiplyStrategyStrassen<long>());
		std::unique_ptr<MatrixMultiplyStrategy<long>> strategy(new MatrixMultiplyStrategyParallel<long>(baseStrategy.get()));

		std::unique_ptr<SquareMatrix<long>> result(SquareMatrix<long>::multiply(&m1, &m2, strategy.get()));

		CHECK(*result == *naiveResult);
	}
}